﻿// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
// Copyright (C) LibreHardwareMonitor and Contributors.
// Partial Copyright (C) Michael Möller <mmoeller@openhardwaremonitor.org> and Contributors.
// All Rights Reserved.

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using Microsoft.Win32.SafeHandles;
using Microsoft.Windows.Sdk;

namespace AsusEC
{
	internal class KernelDriver
	{
		private const uint SC_MANAGER_ALL_ACCESS = 0xF003F;
		private const uint SERVICE_ALL_ACCESS = 0xF01FF;
		private const uint SERVICE_CONTROL_STOP = 1;
		private const int ERROR_SERVICE_ALREADY_RUNNING = unchecked((int)0x80070420);
		private const int ERROR_SERVICE_EXISTS = unchecked((int)0x80070431);

		private readonly string driverId;
		private readonly string serviceName;
		private SafeFileHandle? device;

		public KernelDriver(string serviceName, string driverId)
		{
			this.serviceName = serviceName;
			this.driverId = driverId;
		}

		public bool IsOpen => this.device != null;

		public unsafe void Install(string path)
		{
			SC_HANDLE manager = PInvoke.OpenSCManager((string?)null, null, SC_MANAGER_ALL_ACCESS);
			if (manager == IntPtr.Zero)
			{
				throw new InvalidOperationException("OpenSCManager returned zero.");
			}

			SC_HANDLE service = PInvoke.CreateService(manager,
				this.serviceName,
				this.serviceName,
				SERVICE_ALL_ACCESS,
				EnumServicesStatus_dwServiceType.SERVICE_KERNEL_DRIVER,
				CreateServiceW_dwStartType.SERVICE_DEMAND_START,
				SERVICE_ERROR.SERVICE_ERROR_NORMAL,
				path,
				null,
				null,
				null,
				null,
				null);

			if (service == IntPtr.Zero)
			{
				int error = Marshal.GetHRForLastWin32Error();
				if (error == ERROR_SERVICE_EXISTS)
				{
					throw new InvalidOperationException("Service already exists.");
				}

				string errorMessage = $"CreateService returned the error: {Marshal.GetExceptionForHR(error)?.Message}";
				PInvoke.CloseServiceHandle(manager);
				throw new InvalidOperationException(errorMessage);
			}

			if (!PInvoke.StartService(service, 0, null))
			{
				int error = Marshal.GetHRForLastWin32Error();
				if (error != ERROR_SERVICE_ALREADY_RUNNING)
				{
					string errorMessage = $"StartService returned the error: {Marshal.GetExceptionForHR(error)?.Message}";
					PInvoke.CloseServiceHandle(service);
					PInvoke.CloseServiceHandle(manager);
					throw new InvalidOperationException(errorMessage);
				}
			}

			PInvoke.CloseServiceHandle(service);
			PInvoke.CloseServiceHandle(manager);

			try
			{
				// restrict the driver access to system (SY) and builtin admins (BA)
				// TODO: replace with a call to IoCreateDeviceSecure in the driver
				FileInfo fileInfo = new(@"\\.\" + this.driverId);
				FileSecurity fileSecurity = fileInfo.GetAccessControl();
				fileSecurity.SetSecurityDescriptorSddlForm("O:BAG:SYD:(A;;FA;;;SY)(A;;FA;;;BA)");
				fileInfo.SetAccessControl(fileSecurity);
			}
			catch(Exception ex)
			{
				Debug.WriteLine(ex);
			}
		}

		public bool Open()
		{
			if (this.device != null)
				return true;

			this.device = PInvoke.CreateFile($@"\\.\{this.driverId}",
				(FILE_ACCESS_FLAGS)0xC0000000,
				FILE_SHARE_FLAGS.FILE_SHARE_NONE,
				null,
				FILE_CREATE_FLAGS.OPEN_EXISTING,
				FILE_FLAGS_AND_ATTRIBUTES.FILE_ATTRIBUTE_NORMAL,
				null);

			if (this.device.IsInvalid)
			{
				this.device.Close();
				this.device.Dispose();
				this.device = null;
			}

			return this.device != null;
		}

		public unsafe bool DeviceIOControl(IOControlCode ioControlCode, object? inBuffer)
		{
			if (this.device == null)
				return false;

			uint retBytes;
			if (inBuffer == null)
			{
				return PInvoke.DeviceIoControl(this.device, ioControlCode.Code, null, 0u, null, 0u, &retBytes, null);
			}

			int inSize = Marshal.SizeOf(inBuffer);
			IntPtr inPtr = Marshal.AllocHGlobal(inSize);
			Marshal.StructureToPtr(inBuffer, inPtr, false);

			try
			{
				return PInvoke.DeviceIoControl(this.device, ioControlCode.Code, inPtr.ToPointer(), (uint)inSize, null, 0u, &retBytes, null);
			}
			finally
			{
				Marshal.FreeHGlobal(inPtr);
			}
		}

		public unsafe bool DeviceIOControl<T>(IOControlCode ioControlCode, object? inBuffer, out T? outBuffer)
		{
			outBuffer = default;
			if (this.device == null)
				return false;

			bool result;

			int outSize = Marshal.SizeOf<T>();
			IntPtr outPtr = Marshal.AllocHGlobal(outSize);
			try
			{
				uint retBytes;
				if (inBuffer == null)
				{
					result = PInvoke.DeviceIoControl(this.device, ioControlCode.Code, null, 0u, outPtr.ToPointer(), (uint)outSize, &retBytes, null);
				}
				else
				{
					int inSize = Marshal.SizeOf(inBuffer);
					IntPtr inPtr = Marshal.AllocHGlobal(inSize);
					Marshal.StructureToPtr(inBuffer, inPtr, false);

					try
					{
						result = PInvoke.DeviceIoControl(this.device, ioControlCode.Code, inPtr.ToPointer(), (uint)inSize, outPtr.ToPointer(), (uint)outSize, &retBytes, null);
					}
					finally
					{
						Marshal.FreeHGlobal(inPtr);
					}
				}

				outBuffer = Marshal.PtrToStructure<T>(outPtr);
			}
			finally
			{
				Marshal.FreeHGlobal(outPtr);
			}

			return result;
		}

		public void Close()
		{
			if (this.device == null) 
				return;

			this.device.Close();
			this.device.Dispose();
			this.device = null;
		}

		public bool Delete()
		{
			SC_HANDLE manager = PInvoke.OpenSCManager((string?)null, null, SC_MANAGER_ALL_ACCESS);
			if (manager == IntPtr.Zero)
				return false;
			
			SC_HANDLE service = PInvoke.OpenService(manager, this.serviceName, SERVICE_ALL_ACCESS);
			if (service == IntPtr.Zero)
			{
				PInvoke.CloseServiceHandle(manager);
				return true;
			}

			PInvoke.ControlService(service, SERVICE_CONTROL_STOP, out SERVICE_STATUS status);
			PInvoke.DeleteService(service);
			PInvoke.CloseServiceHandle(service);
			PInvoke.CloseServiceHandle(manager);

			return true;
		}
	}
}
