// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
// Copyright (C) LibreHardwareMonitor and Contributors.
// Partial Copyright (C) Michael Möller <mmoeller@openhardwaremonitor.org> and Contributors.
// All Rights Reserved.

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

namespace AsusEC
{
	internal static class Ring0
	{
		private const uint OLS_TYPE = 40000;

		private static readonly IOControlCode IOCTL_OLS_GET_REFCOUNT = new(OLS_TYPE, 0x801, IOControlCode.Access.Any);
		private static readonly IOControlCode IOCTL_OLS_READ_IO_PORT_BYTE = new(OLS_TYPE, 0x833, IOControlCode.Access.Read);
		private static readonly IOControlCode IOCTL_OLS_WRITE_IO_PORT_BYTE = new(OLS_TYPE, 0x836, IOControlCode.Access.Write);
		
		private static KernelDriver? driver;
		private static Mutex? isaBusMutex;
		
		public static void Open()
		{
			if (driver != null)
				return;

			driver = new KernelDriver("AsusEC_Ring0", "WinRing0_1_2_0");
			driver.Open();

			if (!driver.IsOpen)
			{
				// driver is not loaded, try to install and open
				string filePath = getDriverPath();
				if (!File.Exists(filePath))
					throw new FileNotFoundException("Kernel driver not found", filePath);

				try
				{
					driver.Install(filePath);
				}
				catch (InvalidOperationException)
				{
					driver.Delete();
					Thread.Sleep(2000);
					driver.Install(filePath);
				}

				driver.Open();

				if (!driver.IsOpen)
				{
					driver.Delete();
				}
			}

			if (!driver.IsOpen)
				driver = null;

			const string isaMutexName = "Global\\Access_ISABUS.HTP.Method";

			try
			{
				isaBusMutex = new Mutex(false, isaMutexName);
			}
			catch (UnauthorizedAccessException)
			{
				isaBusMutex = Mutex.OpenExisting(isaMutexName);
			}
		}

		private static string getDriverPath()
		{
			string? myPath = Assembly.GetEntryAssembly()?.Location ?? Process.GetCurrentProcess().MainModule?.FileName;
			myPath = Path.GetDirectoryName(myPath);

			if (myPath == null)
				throw new NotSupportedException("Could not figure out path of exe.");

			return Path.Combine(myPath, "WinRing0x64.sys");
		}

		public static void Close()
		{
			if (driver != null)
			{
				driver.DeviceIOControl(IOCTL_OLS_GET_REFCOUNT, null, out uint refCount);
				driver.Close();

				if (refCount <= 1)
					driver.Delete();

				driver = null;
			}

			if (isaBusMutex != null)
			{
				isaBusMutex.Close();
				isaBusMutex = null;
			}
		}

		public static bool WaitIsaBusMutex(int millisecondsTimeout)
		{
			if (isaBusMutex == null)
				throw new InvalidOperationException("ISA bus mutex not opened.");

			return isaBusMutex.WaitOne(millisecondsTimeout, false);
		}

		public static void ReleaseIsaBusMutex()
		{
			if (isaBusMutex == null)
				throw new InvalidOperationException("ISA bus mutex not opened.");

			isaBusMutex.ReleaseMutex();
		}

		public static byte ReadIoPort(uint port)
		{
			if (driver == null)
				return 0;
			
			driver.DeviceIOControl(IOCTL_OLS_READ_IO_PORT_BYTE, port, out uint value);
			return (byte)(value & 0xFF);
		}

		public static void WriteIoPort(uint port, byte value)
		{
			if (driver == null)
				return;
			
			WriteIoPortInput input = new() { PortNumber = port, Value = value };
			driver.DeviceIOControl(IOCTL_OLS_WRITE_IO_PORT_BYTE, input);
		}

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		private struct WriteIoPortInput
		{
			public uint PortNumber;
			public byte Value;
		}
	}
}

