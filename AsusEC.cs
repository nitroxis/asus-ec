﻿using System;
using System.Buffers.Binary;
using System.Threading;

namespace AsusEC
{
	public static class AsusEC
	{
		public static int? GetTemperature()
		{
			return EmbeddedController.ReadByteLocked(0x3D);
		}

		public static int? GetChipsetFanRpm()
		{
			Span<byte> temp = stackalloc byte[2];
			if (!EmbeddedController.ReadBytesLocked(temp, 0xb4))
				return null;

			return BinaryPrimitives.ReadInt16BigEndian(temp);
		}

		private static void dumpHex(byte[] newBuffer, byte[] oldBuffer)
		{
			Console.Write("   | ");
			for (int i = 0; i < 16; ++i)
			{
				Console.Write($"{i:X2} ");
			}

			Console.WriteLine();
			Console.Write(new string('-', 5 + 16 * 3));

			for (int i = 0; i < newBuffer.Length; ++i)
			{
				if (i % 16 == 0)
				{
					Console.WriteLine();
					Console.Write($"{i:X2} | ");
				}

				if (newBuffer[i] != oldBuffer[i])
				{
					Console.ForegroundColor = ConsoleColor.Cyan;
				}
				else
				{
					Console.ResetColor();
				}

				Console.Write($"{newBuffer[i]:X2} ");
			}
			Console.WriteLine();
		}

		internal static void Main(string[] args)
		{
			bool run = true;

			void onCancel(object? sender, ConsoleCancelEventArgs eventArgs)
			{
				run = false;
				eventArgs.Cancel = true;
				Console.CancelKeyPress -= onCancel;
			}

			Console.CancelKeyPress += onCancel;

			//byte[] prevBuffer = new byte[256];
			//byte[] buffer = new byte[256];

			Ring0.Open();
			try
			{
				while (run)
				{
					Console.CursorTop = 0;

					//if (EmbeddedController.ReadBytesLocked(buffer, 0))
					//{
					//	dumpHex(buffer, prevBuffer);
					//	Console.ResetColor();
					//	Console.WriteLine();

					//	(buffer, prevBuffer) = (prevBuffer, buffer);
					//}

					Console.WriteLine($"T_SENSOR = {GetTemperature()}°C ");
					//Console.WriteLine($"Chipset fan = {GetChipsetFanRpm()} RPM ");
					Console.WriteLine();

					Thread.Sleep(1000);
				}
			}
			finally
			{
				Ring0.Close();
			}
		}
	}
}
