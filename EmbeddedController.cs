﻿// Taken from https://github.com/hirschmann/nbfc (GPL)

using System;

namespace AsusEC
{
	internal static class EmbeddedController
	{
		#region Enums

		// See ACPI specs ch.12.2
		enum ECStatus : byte
		{
			OutputBufferFull = 0x01,    // EC_OBF
			InputBufferFull = 0x02,     // EC_IBF
			// 0x04 is ignored
			Command = 0x08,             // CMD
			BurstMode = 0x10,           // BURST
			SCIEventPending = 0x20,     // SCI_EVT
			SMIEventPending = 0x40      // SMI_EVT
			// 0x80 is ignored
		}

		// See ACPI specs ch.12.3
		enum ECCommand : byte
		{
			Read = 0x80,            // RD_EC
			Write = 0x81,           // WR_EC
			BurstEnable = 0x82,     // BE_EC
			BurstDisable = 0x83,    // BD_EC
			Query = 0x84            // QR_EC
		}

		#endregion

		#region Constants

		private const int mutexLockTimeout = 200;

		private const int commandPort = 0x66;    //EC_SC
		private const int dataPort = 0x62;       //EC_DATA

		private const int rwTimeout = 500;      // spins
		private const int maxRetries = 5;

		#endregion

		#region Methods

		public static byte? ReadByteLocked(byte register)
		{
			bool res = Ring0.WaitIsaBusMutex(mutexLockTimeout);
			if (!res)
				return null;

			try
			{
				return EmbeddedController.readByte(register);
			}
			finally
			{
				Ring0.ReleaseIsaBusMutex();
			}
		}

		public static bool ReadBytesLocked(Span<byte> dest, byte start)
		{
			bool res = Ring0.WaitIsaBusMutex(mutexLockTimeout);
			if (!res)
				return false;

			try
			{
				for (int i = 0; i < dest.Length; ++i)
				{
					byte? b = EmbeddedController.readByte((byte)(start + i));
					if (!b.HasValue)
						return false;

					dest[i] = b.Value;
				}
			}
			finally
			{
				Ring0.ReleaseIsaBusMutex();
			}

			return true;
		}

		private static byte? readByte(byte register)
		{
			int reads = 0;

			while (reads < maxRetries)
			{
				if (tryReadByte(register, out byte result))
				{
					return result;
				}

				reads++;
			}

			return null;
		}

		private static bool tryReadByte(byte register, out byte value)
		{
			if (waitWrite())
			{
				Ring0.WriteIoPort(commandPort, (byte)ECCommand.Read);

				if (waitWrite())
				{
					Ring0.WriteIoPort(dataPort, register);

					if (waitWrite() && waitRead())
					{
						value = Ring0.ReadIoPort(dataPort);
						return true;
					}
				}
			}

			value = 0;
			return false;
		}

		#endregion

		#region Private Methods

		private static bool waitRead()
		{
			return waitForEcStatus(ECStatus.OutputBufferFull, true);
		}

		private static bool waitWrite()
		{
			return waitForEcStatus(ECStatus.InputBufferFull, false);
		}

		private static bool waitForEcStatus(ECStatus status, bool isSet)
		{
			int timeout = rwTimeout;

			while (timeout > 0)
			{
				timeout--;
				byte value = Ring0.ReadIoPort(commandPort);

				if (isSet)
				{
					value = (byte)~value;
				}

				if (((byte)status & value) == 0)
				{
					return true;
				}
			}

			return false;
		}

		#endregion
	}
}