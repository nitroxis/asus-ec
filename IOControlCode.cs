// This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
// Copyright (C) LibreHardwareMonitor and Contributors.
// All Rights Reserved.

using System.Runtime.InteropServices;

namespace AsusEC
{
	[StructLayout(LayoutKind.Sequential, Pack = 1)]
	internal readonly struct IOControlCode
	{
		/// <summary>
		///     Gets the resulting IO control code.
		/// </summary>
		public uint Code { get; }

		/// <summary>
		///     Initializes a new instance of the <see cref="IOControlCode" /> struct.
		/// </summary>
		/// <param name="deviceType">Type of the device.</param>
		/// <param name="function">The function.</param>
		/// <param name="access">The access.</param>
		public IOControlCode(uint deviceType, uint function, Access access) : this(deviceType, function, Method.Buffered, access)
		{
		}

		/// <summary>
		///     Initializes a new instance of the <see cref="IOControlCode" /> struct.
		/// </summary>
		/// <param name="deviceType">Type of the device.</param>
		/// <param name="function">The function.</param>
		/// <param name="method">The method.</param>
		/// <param name="access">The access.</param>
		public IOControlCode(uint deviceType, uint function, Method method, Access access)
		{
			this.Code = (deviceType << 16) | ((uint)access << 14) | (function << 2) | (uint)method;
		}

		public enum Method : uint
		{
			Buffered = 0,
			InDirect = 1,
			OutDirect = 2,
			Neither = 3
		}

		public enum Access : uint
		{
			Any = 0,
			Read = 1,
			Write = 2
		}
	}
}

